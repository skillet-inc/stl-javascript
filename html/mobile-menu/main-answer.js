window.addEventListener('DOMContentLoaded', () => {
  const callback = () => {
    const nav = document.querySelector('.header__mobile-nav');
    nav.classList.toggle('open');
    const line = document.querySelector('.hamburger__line');
    line.classList.toggle('open');
  };

  const el = document.querySelector('.hamburger')
  el.addEventListener('click', callback);
});
