window.addEventListener('DOMContentLoaded', () => {
  // IntersectionObserverのオプション設定
  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 0.1
  };

  //IntersectionObserverのcallback関数の作成
  const callback = (entries, observer) => {
    console.log(entries);
    entries.forEach( entry => {
      if(entry.isIntersecting) {
        // 要素が交差した際の動作
        entry.target.classList.add('show');
      }
    });
  };

  const observer = new IntersectionObserver(callback, options);
  const els = document.querySelectorAll('.target');
  els.forEach((el)=> {
    observer.observe(el);
  })
});
